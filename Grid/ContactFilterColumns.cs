﻿namespace BlazorServerEFCoreSample.Grid
{
    /// <summary>
    /// Sort options.
    /// </summary>
    public enum ContactFilterColumns
    {
        Name,
        Phone,
        Street,
        CityId,
        StateId,
        ZipCode
    }
}
